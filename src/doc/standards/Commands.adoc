= Commands to run during the course

[source,bash]
.commands.sh
----
npm run build // for angular

mvn spring-boot:run // for starting the app locally

mvn clean package // for packaging up the war

mvn cargo:redeploy -Dcargo.hostname="35.164.81.195" -Dcargo.servlet.port="8888" -Dcargo.username="tomcat" -Dcargo.password="tomcat" // deploying the war

// running the cukes tests

mvn -Dtest="RunCukesTest" test -Ddomain=“http://localhost” -Dport="8080" -Dcontext=“safebear” -Dsleep="0"

// running the API tests

mvn clean test -Ddomain=“http://localhost” -Dport="8080" -Dcontext=“safebear”
----