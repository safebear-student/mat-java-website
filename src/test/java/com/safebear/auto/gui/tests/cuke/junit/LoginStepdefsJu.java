package com.safebear.auto.gui.tests.cuke.junit;

import com.cucumber.listener.Reporter;
import com.safebear.auto.gui.tests.pages.LoginPage;
import com.safebear.auto.gui.tests.pages.ToolsPage;
import com.safebear.auto.gui.tests.utils.Utils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class LoginStepdefsJu {

    private LoginPage loginPage;
    private ToolsPage toolsPage;
    private WebDriver driver;

    @Before
    public void setUp(){

        this.driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);

    }

    @After
    public void tearDown(){
        driver.quit();
    }


    @Given("^that I am logged in$")
    public void that_I_am_logged_in() {
        // Step 1 ACTION: open browser and go to the URL, maximise the screen
        driver.get(Utils.getUrl());
        driver.manage().window().maximize();
        // Step 1 RESULT: check we're on the Login Page
        Assertions.assertThat(loginPage.checkPage()).isEqualTo(true);

        // Step 2 ACTION: Enter Username and Password
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");

        // Step 3 ACTION: Press the Login button
        loginPage.clickLoginButton();
        // Step 3 RESULT: check that we're now on the Tools Page
        Assertions.assertThat(toolsPage.checkPage()).isEqualTo(true);

        // Let's also take a screenshot and put it into the report:
        String fileName = Utils.generateScreenShotFileName();
        Utils.capturescreenshot(driver, fileName);
        Reporter.addStepLog("Screenshot showing that we're logged in");
        try {
            Reporter.addScreenCaptureFromPath("../../screenshots/" + fileName, "Logged In");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}