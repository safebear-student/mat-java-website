package com.safebear.auto.gui.tests.cuke.junit;

import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import java.io.File;


// Needs updating with extent reports
@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber/extent_report_ju.html"},
        tags = "~@to-do",
        glue = "com.safebear.auto.gui.tests.cuke.junit",
        features = "classpath:toollist.features/toolmanagement.feature"
)
public class RunCukesJu {

    @AfterClass
    public static void setup() {
        Reporter.loadXMLConfig(new File("src/test/resources/extent-config.xml"));
        Reporter.setSystemInfo("user", System.getProperty("user.name"));
        Reporter.setSystemInfo("os", "Windows");
        Reporter.setTestRunnerOutput("Sample test runner output message");
    }
}
