package com.safebear.auto.gui.tests.cuke.junit;

import com.safebear.auto.gui.tests.pages.AddExPage;
import com.safebear.auto.gui.tests.pages.LoginPage;
import com.safebear.auto.gui.tests.pages.NewToolPage;
import com.safebear.auto.gui.tests.pages.ToolsPage;
import com.safebear.auto.gui.tests.pages.locators.ToolLocators;
import com.safebear.auto.gui.tests.utils.Utils;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class ToolListStepdefsJu {

    private LoginPage loginPage;
    private ToolsPage toolsPage;
    private NewToolPage newToolPage;
    private AddExPage addExPage;
    private WebDriver driver;
    private ToolLocators toolLocator;

    @Before
    public void setUp(){

        this.driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
        newToolPage = new NewToolPage(driver);
        addExPage = new AddExPage(driver);

        // Login and navigate to the Tools Page. This assumes the the Login is working and has been tested - no assertions here.
        driver.get(Utils.getUrl());
        driver.manage().window().maximize();
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();
    }

    @After
    public void tearDown(){

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }



    @Given("^the following tools are created:$")
    public void the_following_tools_are_created(DataTable tools) {

        // Convert the DataTable into a List of Lists (i.e. a list of rows - each row is a tool, so a list of tools)
        List<List<String>> data = tools.raw();

        // for each tool in the list
        for (List<String> tool : data) {
            // create a tool locator out of it
            toolLocator = new ToolLocators(tool.get(0),tool.get(1),tool.get(2));
            // if the tool hasn't already been created, create it
            if(!toolsPage.checkForTool(toolLocator)) {
                toolsPage.clickNewToolButton();
                newToolPage.fillOutFields(toolLocator);
                newToolPage.clickAccept();
            }

        }

    }

    @Then("^the following tools are visible$")
    public void the_following_tools_are_visible(DataTable tools) {

        // Convert the Data Table into a list of Tools as before
        List<List<String>> data = tools.raw();

        // For each tool in the list
        for (List<String> tool : data) {

            // Create a new tool locator
            toolLocator = new ToolLocators(tool.get(0),tool.get(1),tool.get(2));

            // A 'Then' is an Expected Result, so we need an assertion.
            Assertions.assertThat(toolsPage.checkForTool(toolLocator)).isEqualTo(true);

        }

    }

    @When("^a user deletes the (.+) tool$")
    public void a_user_deletes_the_Cucumber_tool(String toolName) {

        // Need to create a new toolLocator - only the 'Name' field is important - put some default text into the other fields.
        toolLocator = new ToolLocators(toolName, "blank","blank");
        toolsPage.deleteTool(toolLocator);

    }

    @Then("^the (.+) tool is deleted$")
    public void the_Cucumber_tool_is_deleted(String toolName) {

        // Need to create a new toolLocator - only the 'Name' field is important - put some default text into the other fields.
        toolLocator = new ToolLocators(toolName, "blank","blank");

        // A 'Then' is an Expected Result, so we need an Assertion to check that the tool is not longer there (returns false).
        Assertions.assertThat(toolsPage.checkForTool(toolLocator)).isEqualTo(false);

    }
}
