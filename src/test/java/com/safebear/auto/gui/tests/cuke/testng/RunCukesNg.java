package com.safebear.auto.gui.tests.cuke.testng;

import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterClass;

import java.io.File;

@CucumberOptions(
        features = {"classpath:toollist.features/toolmanagement.feature"},
        glue = {"com.safebear.auto.gui.tests.cuke.testng"},
        tags = "~@to-do",
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber/extent_report_ng.html"}
)
public class RunCukesNg extends AbstractTestNGCucumberTests {

    @AfterClass
    public static void teardown() {
        Reporter.loadXMLConfig(new File("src/test/resources/extent-config.xml"));
        Reporter.setSystemInfo("user", System.getProperty("user.name"));
        Reporter.setSystemInfo("os", "Windows");
        Reporter.setTestRunnerOutput("Sample test runner output message");
    }


}
