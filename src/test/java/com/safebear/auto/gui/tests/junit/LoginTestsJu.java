package com.safebear.auto.gui.tests.junit;

import com.safebear.auto.gui.tests.pages.LoginPage;
import com.safebear.auto.gui.tests.pages.ToolsPage;
import com.safebear.auto.gui.tests.utils.Utils;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class LoginTestsJu {

    private LoginPage loginPage;
    private ToolsPage toolsPage;
    private WebDriver driver;

    @Before
    public void setUp(){

        this.driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);

    }

    @After
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void login(){

        // Step 1 ACTION: open browser and go to the URL, maximise the screen
        driver.get(Utils.getUrl());
        driver.manage().window().maximize();
        // Step 1 RESULT: check we're on the Login Page
        Assertions.assertThat(loginPage.checkPage()).isEqualTo(true);

        // Step 2 ACTION: Enter Username and Password
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");

        // Step 3 ACTION: Press the Login button
        loginPage.clickLoginButton();
        // Step 3 RESULT: check that we're now on the Tools Page
        Assertions.assertThat(toolsPage.checkPage()).isEqualTo(true);

    }



}
