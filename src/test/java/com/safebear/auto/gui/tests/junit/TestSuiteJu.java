package com.safebear.auto.gui.tests.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        LoginTestsJu.class,
        ToolListTestsJu.class,
})
public class TestSuiteJu {
}
