package com.safebear.auto.gui.tests.junit;

import com.safebear.auto.gui.tests.pages.AddExPage;
import com.safebear.auto.gui.tests.pages.LoginPage;
import com.safebear.auto.gui.tests.pages.NewToolPage;
import com.safebear.auto.gui.tests.pages.ToolsPage;
import com.safebear.auto.gui.tests.pages.locators.ToolLocators;
import com.safebear.auto.gui.tests.utils.Utils;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class ToolListTestsJu {

    private LoginPage loginPage;
    private ToolsPage toolsPage;
    private NewToolPage newToolPage;
    private AddExPage addExPage;
    private WebDriver driver;

    @Before
    public void setUp(){

        this.driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
        newToolPage = new NewToolPage(driver);
        addExPage = new AddExPage(driver);

        // Login and navigate to the Tools Page. This assumes the the Login is working and has been tested - no assertions here.
        driver.get(Utils.getUrl());
        driver.manage().window().maximize();
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();
    }

    @After
    public void tearDown(){

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }

    @Test
    public void createTool(){

        // Step 1: ACTION click on 'New Tool' button
        toolsPage.clickNewToolButton();
        // Step 1: RESULT check we're now on the 'New Tool' page
        Assertions.assertThat(newToolPage.checkPage()).isEqualTo(true);

        // Step 2: ACTION create a new tool entry
        ToolLocators tool = new ToolLocators("Docker","Test Environments", "docker.io" );
        newToolPage.fillOutFields(tool);

        // Step 3: ACTION click on the 'accept' button
        newToolPage.clickAccept();
        // Step 3: RESULT check that we're back on the Tools Page and that the new tool is present
        Assertions.assertThat(toolsPage.checkPage()).isEqualTo(true);
        Assertions.assertThat(toolsPage.checkForTool(tool)).isEqualTo(true);
    }

    @Test
    public void deleteTool(){

        // Step 1: ACTION define which tool we're going to delete and delete it.
        ToolLocators tool = new ToolLocators("Selenium","Web Test Automation", "www.seleniumhq.org" );
        toolsPage.deleteTool(tool);
        // Step 1: RESULT check that tool is deleted.
        Assertions.assertThat(toolsPage.checkForTool(tool)).isEqualTo(false);
    }

}
