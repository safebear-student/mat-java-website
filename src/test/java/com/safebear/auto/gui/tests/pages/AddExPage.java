package com.safebear.auto.gui.tests.pages;

import com.safebear.auto.gui.tests.pages.locators.AddExPageLocators;
import com.safebear.auto.gui.tests.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

@RequiredArgsConstructor
public class AddExPage extends AddExPageLocators {

    @NonNull
    WebDriver driver;

    public boolean checkPage(){

        return driver.getTitle().contains("Additional Exercises");
    }

    public void clickSaySomething(){

        Utils.waitForElement(saysomethingButtonLocator, driver).click();
    }

    public void enterSomethingInAlert(String saySomething){

        driver.switchTo().alert().sendKeys(saySomething);
        driver.switchTo().alert().accept();

    }

    public String checkSaySomethingText(){

        return Utils.waitForElement(saysomethingOutputLocator, driver).getText();

    }

    public void clickGoToFrames(){
        Utils.waitForElement(framesButtonLocator, driver).click();
    }

    public void enterNumbers(int a, int b){
        Select number1 = new Select(Utils.waitForElement(firstdropdownLocator, driver));
        Select number2 = new Select(Utils.waitForElement(seconddropdownFieldLocator, driver));

        number1.selectByVisibleText(Integer.toString(a));
        number2.selectByVisibleText(Integer.toString(b));

    }

    public String checkAddition() {

        return Utils.waitForElement(dropdownOutputLocator, driver).getText();

    }

    public void clickSubmit(){

        Utils.waitForElement(submitButtonLocator, driver).click();

    }




}
