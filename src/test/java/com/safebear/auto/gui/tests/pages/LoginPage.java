package com.safebear.auto.gui.tests.pages;

import com.safebear.auto.gui.tests.pages.locators.LoginPageLocators;
import com.safebear.auto.gui.tests.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage extends LoginPageLocators {

    @NonNull
    private WebDriver driver;

    public boolean checkPage(){

        return driver.getTitle().contains("Login Page");

    }

    public void enterUsername(String username){

        Utils.waitForElement(usernameLocator, driver).sendKeys(username);

    }

    public void enterPassword(String password){

        Utils.waitForElement(passwordLocator, driver).sendKeys(password);

    }

    public void clickLoginButton(){

        Utils.waitForElement(loginButtonLocator, driver).click();

    }

}
