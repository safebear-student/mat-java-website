package com.safebear.auto.gui.tests.pages;

import com.safebear.auto.gui.tests.pages.locators.NewToolPageLocators;
import com.safebear.auto.gui.tests.pages.locators.ToolLocators;
import com.safebear.auto.gui.tests.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

@RequiredArgsConstructor
public class NewToolPage extends NewToolPageLocators {

    @NonNull
    private WebDriver driver;

    public boolean checkPage(){

        return driver.getTitle().contains("New Tool");

    }

    public void fillOutFields(ToolLocators toolLocators){

        Utils.waitForElement(nameFieldLocator, driver).sendKeys(toolLocators.getName());
        Utils.waitForElement(useFieldLocator, driver).sendKeys(toolLocators.getUse());
        Utils.waitForElement(websiteFieldLocator, driver).sendKeys(toolLocators.getWebsite());
    }

    public void clickAccept(){

        Utils.waitForElement(acceptButtonLocator, driver).click();

    }





}
