package com.safebear.auto.gui.tests.pages;

import com.safebear.auto.gui.tests.pages.locators.ToolLocators;
import com.safebear.auto.gui.tests.pages.locators.ToolsPageLocators;
import com.safebear.auto.gui.tests.utils.Utils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.*;

@RequiredArgsConstructor
public class ToolsPage extends ToolsPageLocators {

    @NonNull
    private WebDriver driver;

    public boolean checkPage(){
        return driver.getTitle().contains("Tools Page");
    }

    public boolean checkForTool(ToolLocators toolLocators){

        return Utils.webElementPresent(toolLocators.getNameLocator(), driver);

    }

    public void deleteTool(ToolLocators toolLocators){

        WebElement deleteButton = Utils.waitForElement(toolLocators.getDeleteLocator(), driver);
        deleteButton.click();

   }

    public void clickAdditionalExercisesButton() {

        WebElement addExButton = Utils.waitForElement(additionalExercisesLocator, driver);
        addExButton.click();
   }

    public void clickNewToolButton(){

        WebElement newToolButton = Utils.waitForElement(newToolLocator, driver);
        newToolButton.click();
   }

}
