package com.safebear.auto.gui.tests.pages.locators;

import org.openqa.selenium.By;

public class AddExPageLocators {

    // Locators for the fields, using their ids
    protected By saysomethingOutputLocator = By.id("saySomething");
    protected By firstdropdownLocator = By.id("num1");
    protected By seconddropdownFieldLocator = By.id("num2");
    protected By dropdownOutputLocator = By.id("addResult");

    // Locators for the buttons - using the css selectors
    protected By saysomethingButtonLocator = By.xpath(".//button[contains(text(),'Say Something')]");
    protected By framesButtonLocator = By.partialLinkText("Go to the frames");
    protected By submitButtonLocator = By.xpath(".//input[@value='Submit']");


}
