package com.safebear.auto.gui.tests.pages.locators;

import org.openqa.selenium.By;

public class LoginPageLocators {

    protected By usernameLocator = By.id("username");
    protected By passwordLocator = By.id("password");
    protected By loginButtonLocator = By.id("enter");

}
