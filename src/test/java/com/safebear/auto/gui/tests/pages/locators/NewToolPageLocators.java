package com.safebear.auto.gui.tests.pages.locators;

import org.openqa.selenium.By;

public class NewToolPageLocators {

    // Locators for the fields, using their ids
    protected By nameFieldLocator = By.id("name");
    protected By useFieldLocator = By.id("use");
    protected By websiteFieldLocator = By.id("website");

    // Locators for the buttons - using the css selectors
    protected By acceptButtonLocator = By.xpath(".//button[contains(text(),'Accept')]");


}
