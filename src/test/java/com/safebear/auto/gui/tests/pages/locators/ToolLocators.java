package com.safebear.auto.gui.tests.pages.locators;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.By;

@Data
@RequiredArgsConstructor
public class ToolLocators {

    /**
     * This is where we'll store the attributes of each tool. We've set each
     * attribute to @NotNull so that the @RequiredArgsConstructor turns them
     * into parameters for the tool object and you have to pass them through
     * when a tool object is created.
     *
     * The @Data tag above the class also creates getters and setters for all these.
     */
    @NonNull
    private String name;
    @NonNull
    private String use;
    @NonNull
    private String website;


    /**
     * Let's now create locators for each tool we create. We'll use the 'name' attribute
     * for all the locators as this is the only one which is sure to be unique
     */

    public By getIdLocator() {
        //  Here we're using xpath to find the 'name' cell in the table and then moving to the previous cell to find the ID
        return By.xpath(String.format(".//td[contains(text(),\"%s\")]/preceding-sibling::td", this.name));
    }

    public By getNameLocator(){
        //  Here we're using xpath to find the 'name' cell in the table
        return By.xpath(String.format(".//td[contains(text(),\"%s\")]",this.name));
    }

    public By getUseLocator() {
        //  Here we're using xpath to find the 'name' cell in the table and then moving to the following cell to find the ID
        return By.xpath(String.format(".//td[contains(text(),\"%s\")]/following-sibling::td", this.name));
    }

    public By getwebSiteLocator() {
        return By.xpath(String.format(".//td[contains(text(),\"%s\")]/following-sibling::td[2]", this.name));

    }

    public By getDeleteLocator() {
        // The link to delete the tool is actually inside the cell as a child 'a' element
        return By.xpath(String.format(".//td[contains(text(),\"%s\")]/following-sibling::td[3]/a", this.name));
    }

    /**
     * Note the dot in front of the '//' for all the xpaths. This means that instead of searching the
     * entire document, the xpath limits is search to the children or siblings of the receiving
     * element - speeding up your tests.
     */

}
