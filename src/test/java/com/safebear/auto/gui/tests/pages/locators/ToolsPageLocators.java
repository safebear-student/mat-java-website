package com.safebear.auto.gui.tests.pages.locators;

import org.openqa.selenium.By;

public class ToolsPageLocators {

    /**
     * Locators for the buttons - using the css selectors. This will only work if we have unique classes for
     * each buttons, which is sometimes called 'qa selectors'
     */
    protected By newToolLocator = By.cssSelector(".btn-md");
    protected By additionalExercisesLocator = By.cssSelector(".btn-success");


}
