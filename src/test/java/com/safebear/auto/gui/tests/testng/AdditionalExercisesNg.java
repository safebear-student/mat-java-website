package com.safebear.auto.gui.tests.testng;

import com.safebear.auto.gui.tests.pages.AddExPage;
import com.safebear.auto.gui.tests.pages.LoginPage;
import com.safebear.auto.gui.tests.pages.ToolsPage;
import com.safebear.auto.gui.tests.utils.Utils;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AdditionalExercisesNg {

    private LoginPage loginPage;
    private ToolsPage toolsPage;
    private AddExPage addExPage;
    private WebDriver driver;

    @BeforeTest
    public void setUp(){

        this.driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
        addExPage = new AddExPage(driver);

        // Login and navigate to the Tools Page. This assumes the the Login is working and has been tested - no assertions here.
        driver.get(Utils.getUrl());
        driver.manage().window().maximize();
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();

    }

    @AfterTest
    public void tearDown(){

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();

    }

    @Test
    public void testJavaScriptInput(){

        // Step 1: ACTION Navigate to the page
        toolsPage.clickAdditionalExercisesButton();
        // Step 1: RESULT on correct page
        Assertions.assertThat(addExPage.checkPage()).isEqualTo(true);

        //Step 2: ACTION click on the Say Something button
        addExPage.clickSaySomething();

        //Step 3: ACTION enter some text in the alert
        addExPage.enterSomethingInAlert("hello");
        //Step 3: RESULT check that our text is returned
        Assertions.assertThat(addExPage.checkSaySomethingText()).contains("hello");
    }

    @Test
    public void testFrames(){
        // left empty as a challenge for you! You'll need to create a Frames Page Object for the Frames Page.
    }

    @Test
    public void testDropDowns(){

        // Step 1: ACTION Navigate to the page
        toolsPage.clickAdditionalExercisesButton();
        // Step 1: RESULT on correct page
        Assertions.assertThat(addExPage.checkPage()).isEqualTo(true);

        // Step 2: ACTION Send two numbers to the dropdown boxes
        addExPage.enterNumbers(2, 3);

        // Step 3: ACTION Press the Submit button
        addExPage.clickSubmit();
        // Step 3: RESULT check the result that is returned
        Assertions.assertThat(addExPage.checkAddition()).contains("5");
    }
}
