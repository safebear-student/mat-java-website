package com.safebear.auto.gui.tests.testng;

import com.safebear.auto.gui.tests.pages.LoginPage;
import com.safebear.auto.gui.tests.pages.ToolsPage;
import com.safebear.auto.gui.tests.utils.Utils;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LoginTestsNg {


    private LoginPage loginPage;
    private ToolsPage toolsPage;
    private WebDriver driver;

    @BeforeTest
    public void setUp(){

        this.driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);

    }

    @AfterTest
    public void tearDown(){
        driver.quit();
    }

    // @Test(enabled = false) => this will set TestNG to ignore this test
    // @Test(groups = { "webtest" }) => this will include this test in the 'webtest' group (see the test suite)
    // @Test(dependsOnMethods = { "methodname" }) => This will ensure that a certain method is run before this one. You can also have 'dependsOnGroups' to run a group of tests first.
    // Maybe also have an example here of a data provider reading in a CSV file to run a test multiple times with different data?
    // @Test(priority=0) => You can also specify the order the tests run using 'priority'. '0' will run first, then '1' and so on.
    @Test
    public void login(){

        // Step 1 ACTION: open browser and go to the URL, maximise the screen
        driver.get(Utils.getUrl());
        driver.manage().window().maximize();
        // Step 1 RESULT: check we're on the Login Page
        Assertions.assertThat(loginPage.checkPage()).isEqualTo(true);

        // Step 2 ACTION: Enter Username and Password
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");

        // Step 3 ACTION: Press the Login button
        loginPage.clickLoginButton();
        // Step 3 RESULT: check that we're now on the Tools Page
        Assertions.assertThat(toolsPage.checkPage()).isEqualTo(true);

    }



}
