package com.safebear.auto.gui.tests.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.openqa.selenium.io.FileHandler.copy;

public class Utils {

    private static final String URL = System.getProperty("url","http://localhost:8080");
    private static final String BROWSER = System.getProperty("browser","chrome");

    public static WebDriver getDriver(){

        // This is where we need to say where our chromedriver is (in our 'resources/drivers' folder)
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();

        switch (BROWSER) {

            case "chrome":
                return new ChromeDriver(options);

            case "headless":
                // This time we need the headless option
                options.addArguments("headless");
                return new ChromeDriver(options);

            default:
                return new ChromeDriver(options);

        }
    }

    public static String getUrl(){
        return URL;
    }

    public static String generateScreenShotFileName(){

        // create filename and location
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".png";

    }

    public static void capturescreenshot(WebDriver driver, String fileName) {

        // Take screenshot
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        // Copy file to filename and location we set before
        try {
            copy(scrFile, new File("screenshots/" + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static WebElement waitForElement(By locator, WebDriver driver){

        WebDriverWait wait = new WebDriverWait(driver, 5);

        WebElement element;

        try{

            element = driver.findElement(locator);

        } catch (TimeoutException e){

            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
            element = driver.findElement(locator);

        } catch (Exception e) {

            System.out.println(e.toString());
            capturescreenshot(driver, generateScreenShotFileName());
            throw (e);

        }

        return element;

    }

    public static boolean webElementPresent(By locator, WebDriver driver){

        WebDriverWait wait = new WebDriverWait(driver, 5);

        try{

            driver.findElement(locator);

        } catch (TimeoutException e){

            wait.until(ExpectedConditions.visibilityOfElementLocated(locator));

        } catch (Exception e) {

            System.out.println(e.toString());
            return false;

        }

        return true;

    }

}
