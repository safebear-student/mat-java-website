Feature: Tool Management

  User Story:
  In order to manage my list of automation tools
  As a user
  I must be able to view, add, delete, archive or update tools in the list

  Rules / Acceptance Criteria:
    1. You must be logged in in order to view the tools list
    2. The tools must have the following details:
      a. Name
      b. Use
      c. Date Added (UK format only)
      d. Rating
      e. Company
      f. Website
    3. You must be able to add a tool
    4. You must be able to delete a tool
    5. You must be able to update a tool
    6. You must be able to archive a tool

  Questions:
    1. Do we need pagination?
    2. Can 'Date Added' be in the past?

  To Do:
    6. Archive a tool

  Domain language:
    1. A 'Tool' is a test application that automates your testing. E.g. Selenium, Test Automation, 20-12-1-2018, Good, SeleniumHQ, www.seleniumhq.com

  Background:
    Given that I am logged in
    And the following tools are created:
    |Docker|Test Environments|www.docker.com|
    |Cucumber|BDD            |cucumber.io |
    # Name, Use, Added Date, Rating, Company, Website

  Scenario: A user views the list of tools
    Then the following tools are visible
    |Docker|Test Environments|www.docker.com|
    |Cucumber|BDD            |cucumber.io |

  Scenario Outline: A user deletes a tool
    When a user deletes the <tool> tool
    Then the <tool> tool is deleted
    Examples:
      | tool |
      | Docker |
      | Cucumber|

  @to-do
  @high-impact
  Scenario Outline: A user updates a tool
    When the <tool> tool rating is updated to <new_rating>
    # Using the same step def as the first scenario
    Then the <tool> tools has the new <new_rating> rating
    Examples:
      | tool | new_rating |
      |Cucumber|Excellent|

  @to-do
  @high-impact
  Scenario Outline: A user archives a tool
    When a user archives the <tool> tool
    Then the <tool> tool is archived
    Examples:
      | tool |
      | Docker |
      | Cucumber|